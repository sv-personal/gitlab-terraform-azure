$dockerUser = "steevaavoo"
Push-Location .\docker
$tag = (Get-Date -Format "yyyy-MM-dd")
$dockerImage = "$dockerUser/azbuildagent"
$dockerImageAndTag = "$($dockerImage):$tag"
$dockerImageAndLatestTag = "$($dockerImage):latest"
docker build . -t $dockerImageAndTag
docker tag $dockerImageAndTag $dockerImageAndLatestTag

# Show
docker image ls $dockerUser/azbuildagent

# Push
docker push $dockerUser/azbuildagent:$tag && docker push $dockerUser/azbuildagent:latest

# Run
docker run --rm -it --name az-buildagent $dockerUser/azbuildagent:latest pwsh
