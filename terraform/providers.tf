# Providers (pin all versions)
# Terraform Remote State
terraform {
  required_version = ">= 0.13"
  backend "azurerm" {
    # storage_account_name = "__TERRAFORM_STORAGE_ACCOUNT__"
    # container_name       = "terraform"
    key = "terraform.tfstate"
    # access_key           = "__STORAGE_KEY__"
  }
  required_providers {
    # https://github.com/hashicorp/terraform-provider-helm/releases
    # helm = "2.0.2"
    # https://github.com/hashicorp/terraform-provider-kubernetes/releases
    # kubernetes = "2.0.2"
    # https://registry.terraform.io/providers/hashicorp/azurerm/latest
    # trying latest, but put back to 1.44.0 if this breaks things.
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "2.78.0"
    }
  }
}

provider "azurerm" {
  features {}
}
