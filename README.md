# gitlab-terraform-azure

## Overview

Mapping across my Azure Terraform Modules from GitHub Actions to GitLab CI.

## Tasks

- [x] Get CI/CD working in GitLab Repo
- [x] Build custom docker image
  - [x] Install required packages
  - [x] Push to repository
  - [x] Change pipeline to use custom image - clean up before_script section
  - [ ] Consider using multi-stage Docker image
    See: <https://github.com/PowerShell/PowerShell-Docker/blob/45d96c55986e073e836896a2818f442c553ee808/release/stable/alpine314/docker/Dockerfile#L27>
    Lines 6-30 contain the pre-build environment for the container
- [x] Concatenate and streamline all variables
- [x] Add meaningful comments to gitlab-ci.yml
- [ ] Recreate full Jenkins CI/CD Azure pipeline here
- [ ] Test a Module or two

## Notes

### Setting up CI/CD in GitLab to use Azure

```powershell
# Login to your target Azure environment
az login

# Create a Service Principle named "gitlab-cicd"
# outputting the required info for future use
az ad sp create-for-rbac --name gitlab-cicd --query "{ client_id: appId, client_secret: password, tenant_id: tenant }"

# output subscription id
az account show --query "{ subscription_id: id }"

# Paste these details into the appropriate variables in Settings > CI/CD > Variables in your GitLab Repo
# ticking the Masked option for any sensitive variables.

# Clean up storage account when finished
az group list
az group delete --name "PASTE_NAME_HERE" --yes
```

### Observations

#### Base Docker Image

If using Azure - as we are with this pipeline - it's more straightforward to start with the Azure Docker image from
mcr on Dockerhub, then add the minimum-required packages to enable the container to run the commands we need - such
as the terraform cli and if necessary, kubectl and docker - or anything else that is needed, whilst trying to keep
as lightweight as possible.

#### Custom Docker Image

I am building a custom docker image to use for the pipeline. One thing to note is that `docker login` does not work
properly in an elevated shell, so make sure to do any Docker repository pushes in a standard shell.
<https://github.com/docker/cli/issues/2682>

```powershell
# Building and pushing custom Docker Image
# https://hub.docker.com/r/steevaavoo/azbuildagent

# Build dated and latest tags
$dockerUser = "steevaavoo"
Push-Location .\docker
$tag = (Get-Date -Format "yyyy-MM-dd")
$dockerImage = "$dockerUser/azbuildagent"
$dockerImageAndTag = "$($dockerImage):$tag"
$dockerImageAndLatestTag = "$($dockerImage):latest"
docker build . -t $dockerImageAndTag
docker tag $dockerImageAndTag $dockerImageAndLatestTag

# Show
docker image ls $dockerUser/azbuildagent

# Push
docker push $dockerUser/azbuildagent:$tag ; docker push $dockerUser/azbuildagent:latest

# Run
docker run --rm -it --name jenkins-agent $dockerUser/azbuildagent:latest pwsh

# Run with mounted volume
docker run --rm -it -v ${PWD}:/data --workdir=/data --name azbuildagent $dockerUser/azbuildagent:latest pwsh
```

#### Sharing Dynamic Variables between Jobs

One cannot dynamically update Environment Variables in GitLab CI like in Jenkins/GitHub Actions.

So in the scenario where it would be beneficial to share a variable between build agents/runners (such as a storage
key for terraform backend storage in an Azure Storage Group, for example), where previously we would create a
placeholder Environment Variable in the CI/CD settings (or equivalent area) with placeholder text, then overwrite
that variable in the pipeline at runtime from the get_storage_key script - using, for
example in GitHub Actions `echo "STORAGE_KEY=$storage_key" >> "$GITHUB_ENV"` - we have to instead use "caching" in
GitLab CI. See the yml and relevant comments to see how it was implemented.
