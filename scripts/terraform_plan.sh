#!/bin/bash

set -euo pipefail
trap "echo 'error: Script failed: see failed command above'" ERR

# https://www.terraform.io/docs/commands/plan.html
# Using -out=PATH to store plan

message="Terraform Plan"
echo "STARTED: $message"
terraform -chdir="$TERRAFORM_FOLDER" plan -out=tfplan
echo "FINISHED: $message."
