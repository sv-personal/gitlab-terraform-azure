#!/bin/bash

set -euo pipefail
trap "echo 'error: Script failed: see failed command above'" ERR

message="Getting Storage Account Key"
echo "STARTED: $message..."
storage_key=$(az storage account keys list --resource-group "$TERRAFORM_STORAGE_RG" --account-name "$TERRAFORM_STORAGE_ACCOUNT" --query [0].value --output tsv)
echo "FINISHED: $message."

message="Writing Storage Account Key to cache"
echo "STARTED: $message..."
echo "$storage_key" > ./cache/storage_key.txt
echo "FINISHED: $message."
